# Thu thập thông tin

## Đối tượng
Có 3 đối tượng mà một PM cần thu thập thông tin
- Khách hàng
- Team
- Đối thủ cạnh tranh

## Quy trình thu thập thông tin

- Vòng thu thập nông
    - Bắt đầu từ vấn đề của KH, bạn cần hiểu sơ bộ cụ thể
    vấn đề của KH là gì, KH đang cần sơ bộ là gì.
    - Sau đó bạn cần quay ngược lại thu thập năng lực của sản phầm,
    và năng lực của team bạn (về nhân lực, về tài chính) đang ntn.
    - Tiếp đến cần tìm hiểu về các đối thủ cạnh tranh của bạn trên
    thị trường, những điểm mạnh và điểm yếu của họ

=> Thường vòng này PM mới chỉ bắt đầu manh nha biết vấn đề là gì,
và collect các thông tin liên quan để tiến hành tìm hiểu sâu hơn về 
thứ có thể thành project

- Vòng thu thập sâu:
    - Sau khi có một cái nhìn tổng quan về các bên có thể tham gia dự án,
    PM cần tiếp xúc sâu hơn với KH để tìm cách lấy thêm thông tin từ phía KH.
      - Mục tiêu: 
        - Tìm hiểu nhu cầu thật của KH
        - Đánh giá khả năng của KH
      - Cần áp dụng kỹ năng giao tiếp mềm mỏng nhưng hiệu quả của PM ở giai đoạn này
    - Cân nhắc khả năng của team và khả năng vận dụng các nguồn lực khác ngoài team.
    - Cân nhắc khả năng can thiệp và cạnh tranh cảu đối thủ
=> Sau bước này PM cần có đủ thông tin để đưa ra đánh giá và đủ thông tin để lọc

## Đánh giá thông tin thu thập được
- Đánh giá về độ tin cậy của thông tin
- Đánh giá về dự án:
    - Thời gian dự kiến hoàn thành project:
        - thời gian càng ngắn, nguồn lực của team đổ vào càng nhiều.
    - Scope của dự án
        - Dự án càng to càng nhiều rủi ro và cần nhiều nguồn lực hơn.
        - Cân nhắc xem dự án cần có scope như thế nào thì phù hợp ?
    - Lợi ích từ dự án:
        - Lợi ích về tài chính qua các mốc thời gian
        - Khả năng hoàn thành dự án và khả năng FAIL. 
            - luôn chuẩn bị cho tình hưống dự án FAIL
    
## Báo cáo dự án
- Phần này mình thấy thầy chưa đi sâu
- Về cơ bản thì cần phải đưa ra một bảng các thông sô cụ thể.
Phần này lười chụp cái ảnh quá, làm sau.

    