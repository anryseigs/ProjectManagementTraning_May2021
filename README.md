# ProjectManagementTraning_May2021

## Project manager là ai ? Làm gì ??

Có nhiều định nghĩa nhưng về cơ bản Project Manager là người thu thập thông tin, lên kế hoạch, phân bổ nguồn lực
thực hiện dự án, đưa dự án đến đến đích đã được đề ra. 

Project Manager là người quản lý dự án dựa trên các yếu tố
- đầu vào của dự án
- mong muốn của dự án
- quản lý các thay đổi và cân bằng lợi ích các bên trong quá trình thực hiện dự án.

Project Manager là người điều hoà giữa 3 đối tượng
- Nhu cầu của KH
- Năng lực của team
- Sự cạnh tranh của các đối thủ khác trên thị trường. 

## Thực hiện một project thì PM cần làm những gì ?

Thực hiện một dự án là thực hiện một chu trình gồm các bước:
- [Day 1 - Thu thập thông tin, xử lý thông tin và tổ chức báo cáo thông tin]()
- [Day 2 - Tổ chức, bố trí công viêc - PENDING]()